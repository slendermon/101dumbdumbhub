---
id: 40
title: 'We Need More tor Nodes'
date: '2022-05-24T02:44:00+00:00'
author: 'Ben R'
layout: post
guid: 'https://randomsource.club/?p=40'
permalink: /2022/05/24/we-need-more-tor-nodes/
categories:
    - 'Privacy &amp; Security'
---

Tor only currently has 6000 relays and 2000 bridges. Who knows how many of them can easily be made by the government agencies. Tor is our defense towards censorship. And we can start making more relays and bridges to help decentralize the internet.