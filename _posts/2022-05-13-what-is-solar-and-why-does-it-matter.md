---
id: 25
title: 'What is solar? And why does it matter?'
date: '2022-05-13T21:01:00+00:00'
author: 'Ben R'
layout: post
guid: 'https://randomsource.club/?p=25'
permalink: /2022/05/13/what-is-solar-and-why-does-it-matter/
categories:
    - 'Green Tech'
---

There are many ways to get electricity, especially from sources of coal and nonrenewable resources. But who knows if we could ever get out of this debt of ever ending energy, that we will one day have to rely on renewable sources, like solar and hydroelectric and wind. Solar is the most budget conscious friendly out of all. And where will all of this electricity go to if we hog all of the resources anyway. Over using water and over using materials just to make renewable energy.